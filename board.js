let startLeft;
let startTop;
const whiteKing = new King(true, 4, 7);
const blackKing = new King(false, 4, 0);
let whitesMove = true;
let someoneCanMove = true;
/**
 * For the pieces
 */
function ifLegalDraw(isWhite, left, top){
            let kurzspeicher;
            let kurzLeft;
            let kurzTop;
      if(left > -1 && top > -1 && left < 8 && top < 8){
            if(chessboard[left][top] == null){
                  kurzLeft = startLeft;
                  kurzTop = startTop;
                  chessboard[left][top] = chessboard[startLeft][startTop];
                  chessboard[left][top].left = left;
                  chessboard[left][top].top = top;
                  chessboard[startLeft][startTop] = null;
                  if(isWhite){
                        if(!whiteKing.isInCheck()){
                              paintSquare(left, top);
                        }
                  }
                  else{
                        if(!blackKing.isInCheck()){
                              paintSquare(left, top);
                        }
                  }
                  chessboard[startLeft][startTop] = chessboard[left][top];
                  chessboard[startLeft][startTop].left = kurzLeft;
                  chessboard[startLeft][startTop].top = kurzTop;
                  chessboard[left][top] = null;
            }
            else{
                  if(chessboard[left][top].isWhite !== isWhite){
                        kurzspeicher = chessboard[left][top];
                        kurzLeft = startLeft;
                        kurzTop = startTop;
                        chessboard[left][top] = chessboard[startLeft][startTop];
                        chessboard[left][top].left = left;
                        chessboard[left][top].top = top;
                        chessboard[startLeft][startTop] = null;
                        if(isWhite){
                              if(!whiteKing.isInCheck()){
                               paintSquare(left, top);
                              }
                        }
                        else{
                              if(!blackKing.isInCheck()){
                                    paintSquare(left, top);
                              }
                        }
                        chessboard[startLeft][startTop] = chessboard[left][top];
                        chessboard[startLeft][startTop].left = kurzLeft;
                        chessboard[startLeft][startTop].top = kurzTop;
                        chessboard[left][top] = kurzspeicher;
                  }
            }
      }
}
/**
 * For Rook Bishop and Queen
 */
function goStraightLine(left, top, isWhite, dirleft, dirtop){
      let isblocked = false;
      while(left > -1 && left < 8 && top > -1 && top < 8 && !isblocked){
          //  paintSquare(left, top);
            ifLegalDraw(isWhite, left, top);
            if(chessboard[left][top] !== null){
                  isblocked = true;
            }
            left = left + dirleft;
            top = top + dirtop;
      }
}
/**
 * Checkfunction for Rook Bishop and Queen
 */
 function checkStraightLine(left, top, isWhite, dirleft, dirtop, startfl, startft){
      let isblocked = false;
      while(left > -1 && left < 8 && top > -1 && top < 8 && !isblocked){
            if(ifCanMove(isWhite, left, top, startfl, startft)){ isblocked = true; return true;}
            if(chessboard[left][top] !== null){
                  isblocked = true;
            }
            left = left + dirleft;
            top = top + dirtop;
      }
      return false;
}
/**
 * Can the piece move?
 */
 function ifCanMove(isWhite, left, top, startfl, startft){
      let kurzspeicher;
      let kurzLeft;
      let kurzTop;
if(left > -1 && top > -1 && left < 8 && top < 8){
      if(chessboard[left][top] == null){
            kurzLeft = startfl;
            kurzTop = startft;
            chessboard[left][top] = chessboard[startfl][startft];
            chessboard[left][top].left = left;
            chessboard[left][top].top = top;
            chessboard[startfl][startft] = null;
            if(isWhite){
                  if(!whiteKing.isInCheck()){
                        chessboard[startfl][startft] = chessboard[left][top];
                        chessboard[startfl][startft].left = kurzLeft;
                        chessboard[startfl][startft].top = kurzTop;
                        chessboard[left][top] = null;
                        return true;
                  }
            }
            else{
                  if(!blackKing.isInCheck()){
                        chessboard[startfl][startft] = chessboard[left][top];
                        chessboard[startfl][startft].left = kurzLeft;
                        chessboard[startfl][startft].top = kurzTop;
                        chessboard[left][top] = null;
                        return true;
                  }
            }
            chessboard[startfl][startft] = chessboard[left][top];
            chessboard[startfl][startft].left = kurzLeft;
            chessboard[startfl][startft].top = kurzTop;
            chessboard[left][top] = null;
      }
      else{
            if(chessboard[left][top].isWhite !== isWhite){
                  kurzspeicher = chessboard[left][top];
                  kurzLeft = startfl;
                  kurzTop = startft;
                  chessboard[left][top] = chessboard[startfl][startft];
                  chessboard[left][top].left = left;
                  chessboard[left][top].top = top;
                  chessboard[startfl][startft] = null;
                  if(isWhite){
                        if(!whiteKing.isInCheck()){
                              chessboard[startfl][startft] = chessboard[left][top];
                              chessboard[startfl][startft].left = kurzLeft;
                              chessboard[startfl][startft].top = kurzTop;
                              chessboard[left][top] = kurzspeicher;
                              return true;
                        }
                  }
                  else{
                        if(!blackKing.isInCheck()){
                              chessboard[startfl][startft] = chessboard[left][top];
                              chessboard[startfl][startft].left = kurzLeft;
                              chessboard[startfl][startft].top = kurzTop;
                              chessboard[left][top] = kurzspeicher;
                              return true;
                        }
                  }
                  chessboard[startfl][startft] = chessboard[left][top];
                  chessboard[startfl][startft].left = kurzLeft;
                  chessboard[startfl][startft].top = kurzTop;
                  chessboard[left][top] = kurzspeicher;
            }
      }
}
return false;
}
/**
 * Create King
 */
function King(isWhite, left, top){
      function ifIsKnight(lefts, tops, white){
            if(lefts < 0){return false;}
            if(lefts > 7){return false;}
            if(tops < 0){return false;}
            if(tops > 7){return false;}
            if(chessboard[lefts][tops] == null){
                  return false;
            }
            if(chessboard[lefts][tops].name !== "Knight"){
                  return false;
            }
            if(chessboard[lefts][tops].isWhite == white){
                  return false;
            }
            return true;
      }
      function ifIsPawn(lefts, tops, white){
            if(lefts < 0){return false;}
            if(lefts > 7){return false;}
            if(tops < 0){return false;}
            if(tops > 7){return false;}
            if(chessboard[lefts][tops] == null){
                  return false;
            }
            if(chessboard[lefts][tops].name !== "Pawn"){
                  return false;
            }
            if(chessboard[lefts][tops].isWhite == white){
                  return false;
            }
            return true;
      }
      function ifIsRook(lefts, tops, White, dirleft, dirtop){
            let isblocked = false;
            while(lefts > -1 && lefts < 8 && tops > -1 && tops < 8 && !isblocked){
                  if(chessboard[lefts][tops] !== null){
                        isblocked = true;
                        if((chessboard[lefts][tops].name == "Rook" && chessboard[lefts][tops].isWhite !== White)||(chessboard[lefts][tops].name == "Queen" && chessboard[lefts][tops].isWhite !== White)){
                              return true;
                        }
                  }
                  lefts = lefts + dirleft;
                  tops = tops + dirtop;
            }
            return false;
      }
      function ifIsBishop(lefts, tops, White, dirleft, dirtop){
            let isblocked = false;
            while(lefts > -1 && lefts < 8 && tops > -1 && tops < 8 && !isblocked){
                  if(chessboard[lefts][tops] !== null){
                        isblocked = true;
                        if((chessboard[lefts][tops].name == "Bishop" && chessboard[lefts][tops].isWhite !== White)|| (chessboard[lefts][tops].name == "Queen" && chessboard[lefts][tops].isWhite !== White)){
                              return true;
                        }
                  }
                  lefts = lefts + dirleft;
                  tops = tops + dirtop;
            }
            return false;
      }
      this.isWhite = isWhite;
      this.left = left;
      this.top = top;
      this.hasMoved = false;
      this.name = "king";
      this.showPossibleMoves = function(){
            ifLegalDraw(this.isWhite, this.left-1, this.top-1);
            ifLegalDraw(this.isWhite, this.left-1, this.top);
            ifLegalDraw(this.isWhite, this.left-1, this.top+1);
            ifLegalDraw(this.isWhite, this.left, this.top-1);
            ifLegalDraw(this.isWhite, this.left, this.top+1);
            ifLegalDraw(this.isWhite, this.left+1, this.top-1);
            ifLegalDraw(this.isWhite, this.left+1, this.top);
            ifLegalDraw(this.isWhite, this.left+1, this.top+1);
      }
      this.isInCheck = function(){
            //Does a Knight give check?
            if(ifIsKnight(this.left-2, this.top-1, this.isWhite)){return true;}
            if(ifIsKnight(this.left-2, this.top+1, this.isWhite)){return true;}
            if(ifIsKnight(this.left-1, this.top-2, this.isWhite)){return true;}
            if(ifIsKnight(this.left-1, this.top+2, this.isWhite)){return true;}
            if(ifIsKnight(this.left+1, this.top-2, this.isWhite)){return true;}
            if(ifIsKnight(this.left+1, this.top+2, this.isWhite)){return true;}
            if(ifIsKnight(this.left+2, this.top-1, this.isWhite)){return true;}
            if(ifIsKnight(this.left+2, this.top+1, this.isWhite)){return true;}
            //Does a Pawn give check?
            if(this.isWhite){
                  if(ifIsPawn(this.left-1, this.top-1, this.isWhite)){return true;}
                  if(ifIsPawn(this.left+1, this.top-1, this.isWhite)){return true;}
            }
            else{
                  if(ifIsPawn(this.left-1, this.top+1, this.isWhite)){return true;}
                  if(ifIsPawn(this.left+1, this.top+1, this.isWhite)){return true;}
            }
            //Does a Rook or Queen give Check?
            if(ifIsRook(this.left-1, this.top, this.isWhite, -1, 0)){return true;}  
            if(ifIsRook(this.left+1, this.top, this.isWhite, 1, 0)){return true;}   
            if(ifIsRook(this.left, this.top-1, this.isWhite, 0, -1)){return true;}   
            if(ifIsRook(this.left, this.top+1, this.isWhite, 0, 1)){return true;}    
            //Does a Bishop or Queen give Check?
            if(ifIsBishop(this.left-1, this.top-1, this.isWhite, -1, -1)){return true;}  
            if(ifIsBishop(this.left-1, this.top+1, this.isWhite, -1, 1)){return true;}   
            if(ifIsBishop(this.left+1, this.top+1, this.isWhite, 1, 1)){return true;}   
            if(ifIsBishop(this.left+1, this.top-1, this.isWhite, 1, -1)){return true;}   
            
            return false;
      }
      this.canMove = function(){
            if(ifCanMove(this.isWhite, this.left-1, this.top-1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left-1, this.top, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left-1, this.top+1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left, this.top-1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left, this.top+1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+1, this.top-1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+1, this.top, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+1, this.top+1, this.left, this.top)){return true;}
            return false;
      }
}
/**
 * Create Queen
 */
 function Queen(isWhite, left, top){
      this.isWhite = isWhite;
      this.left = left;
      this.top = top;
      this.hasMoved = false;
      this.name = "Queen";
      this.showPossibleMoves = function(){
            goStraightLine(this.left-1, this.top-1, this.isWhite, -1, -1);
            goStraightLine(this.left-1, this.top, this.isWhite, -1, 0);
            goStraightLine(this.left-1, this.top+1, this.isWhite, -1, 1);
            goStraightLine(this.left, this.top-1, this.isWhite, 0, -1);
            goStraightLine(this.left, this.top+1, this.isWhite, 0, 1);
            goStraightLine(this.left+1, this.top-1, this.isWhite, 1, -1);
            goStraightLine(this.left+1, this.top, this.isWhite, 1, 0);
            goStraightLine(this.left+1, this.top+1, this.isWhite, 1, 1);
      }
      this.canMove = function(){
            if(checkStraightLine(this.left-1, this.top-1, this.isWhite, -1, -1,this.left, this.top)){return true;}
            if(checkStraightLine(this.left-1, this.top, this.isWhite, -1, 0, this.left, this.top)){return true;}
            if(checkStraightLine(this.left-1, this.top+1, this.isWhite, -1, 1, this.left, this.top)){return true;}
            if(checkStraightLine(this.left, this.top-1, this.isWhite, 0, -1, this.left, this.top)){return true;}
            if(checkStraightLine(this.left, this.top+1, this.isWhite, 0, 1, this.left, this.top)){return true;}
            if(checkStraightLine(this.left+1, this.top-1, this.isWhite, 1, -1, this.left, this.top)){return true;}
            if(checkStraightLine(this.left+1, this.top, this.isWhite, 1, 0, this.left, this.top)){return true;}
            if(checkStraightLine(this.left+1, this.top+1, this.isWhite, 1, 1, this.left, this.top)){return true;}
            return false;
      }
}
/**
 * Create Rook
 */
 function Rook(isWhite, left, top){
      this.isWhite = isWhite;
      this.hasMoved = false;
      this.left = left;
      this.top = top;
      this.name = "Rook";
      this.showPossibleMoves = function(){
            goStraightLine(this.left, this.top-1, this.isWhite, 0, -1);
            goStraightLine(this.left-1, this.top, this.isWhite, -1, 0);
            goStraightLine(this.left+1, this.top, this.isWhite, 1, 0);
            goStraightLine(this.left, this.top+1, this.isWhite, 0, +1);
      }
      this.canMove = function(){
            if(checkStraightLine(this.left, this.top-1, this.isWhite, 0, -1,this.left, this.top)){return true;}
            if(checkStraightLine(this.left-1, this.top, this.isWhite, -1, 0, this.left, this.top)){return true;}
            if(checkStraightLine(this.left+1, this.top, this.isWhite, 1, 0, this.left, this.top)){return true;}
            if(checkStraightLine(this.left, this.top+1, this.isWhite, 0, +1, this.left, this.top)){return true;}
            return false;
      }
}
/**
 * Create Knight
 */
 function Knight(isWhite, left, top){
      this.isWhite = isWhite;
      this.left = left;
      this.top = top;
      this.hasMoved = false;
      this.name = "Knight";
      this.showPossibleMoves = function(){
            ifLegalDraw(this.isWhite, this.left-2, this.top+1);
            ifLegalDraw(this.isWhite, this.left-2, this.top-1);
            ifLegalDraw(this.isWhite, this.left-1, this.top+2);
            ifLegalDraw(this.isWhite, this.left-1, this.top-2);
            ifLegalDraw(this.isWhite, this.left+1, this.top+2);
            ifLegalDraw(this.isWhite, this.left+1, this.top-2);
            ifLegalDraw(this.isWhite, this.left+2, this.top+1);
            ifLegalDraw(this.isWhite, this.left+2, this.top-1);
      }
      this.canMove = function(){
            if(ifCanMove(this.isWhite, this.left-2, this.top+1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left-2, this.top-1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left-1, this.top+2, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left-1, this.top-2, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+1, this.top+2, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+1, this.top-2, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+2, this.top+1, this.left, this.top)){return true;}
            if(ifCanMove(this.isWhite, this.left+2, this.top-1, this.left, this.top)){return true;}
            return false;
      }
}
/**
 * Create Bishop
 */
 function Bishop(isWhite, left, top){
      this.isWhite = isWhite;
      this.left = left;
      this.top = top;
      this.hasMoved = false;
      this.name = "Bishop";
      this.showPossibleMoves = function(){
            goStraightLine(this.left-1, this.top-1, this.isWhite, -1, -1);
            goStraightLine(this.left+1, this.top-1, this.isWhite, 1, -1);
            goStraightLine(this.left-1, this.top+1, this.isWhite, -1, 1);
            goStraightLine(this.left+1, this.top+1, this.isWhite, 1, 1);
      }
      this.canMove = function(){
            if(checkStraightLine(this.left-1, this.top-1, this.isWhite, -1, -1,this.left, this.top)){return true;}
            if(checkStraightLine(this.left+1, this.top-1, this.isWhite, 1, -1, this.left, this.top)){return true;}
            if(checkStraightLine(this.left-1, this.top+1, this.isWhite, -1, 1, this.left, this.top)){return true;}
            if(checkStraightLine(this.left+1, this.top+1, this.isWhite, 1, 1, this.left, this.top)){return true;}
            return false;
      }
}
/**
 * Create Pawn
 */
 function Pawn(isWhite, left, top){
      this.isWhite = isWhite;
      this.left = left;
      this.top = top;
      this.hasMoved = false;
      this.name = "Pawn";
      this.showPossibleMoves = function(){
            if(this.isWhite){
                  if(chessboard[this.left][this.top-1] == null){
                        ifLegalDraw(this.isWhite, this.left, this.top-1);
                        if(!this.hasMoved && chessboard[this.left][this.top-2] == null){
                              ifLegalDraw(this.isWhite, this.left, this.top-2);
                        }
                  }
                  if(left !== 0){
                        if(chessboard[this.left-1][this.top-1] !== null){
                              if(chessboard[this.left-1][this.top-1].isWhite !== this.isWhite){
                                    ifLegalDraw(this.isWhite, this.left-1, this.top-1);
                              }
                        }
                  }
                  if(left !== 7){
                        if(chessboard[this.left+1][this.top-1] !== null){
                              if(chessboard[this.left+1][this.top-1].isWhite !== this.isWhite){
                                    ifLegalDraw(this.isWhite, this.left+1, this.top-1);
                              }
                        }
                  }
            }
            else{
                  if(chessboard[this.left][this.top+1] == null){
                        ifLegalDraw(this.isWhite, this.left, this.top+1);
                        if(!this.hasMoved && chessboard[this.left][this.top+2] == null){
                              ifLegalDraw(this.isWhite, this.left, this.top+2);
                        }
                  }
                  if(left !== 0){
                        if(chessboard[this.left-1][this.top+1] !== null){
                              if(chessboard[this.left-1][this.top+1].isWhite !== this.isWhite){
                                    ifLegalDraw(this.isWhite, this.left-1, this.top+1);
                              }
                        }
                  }
                  if(left !== 7){
                        if(chessboard[this.left+1][this.top+1] !== null){
                              if(chessboard[this.left+1][this.top+1].isWhite !== this.isWhite){
                                    ifLegalDraw(this.isWhite, this.left+1, this.top+1);
                              }
                        }    
                  }             
            }
      }
      this.canMove = function(){
            if(this.isWhite){
                  if(chessboard[this.left][this.top-1] == null){
                        if(ifCanMove(this.isWhite, this.left, this.top-1, this.left, this.top)){return true;}
                        if(!this.hasMoved && chessboard[this.left][this.top-2] == null){
                              if(ifCanMove(this.isWhite, this.left, this.top-2, this.left, this.top)){return true;}
                        }
                  }
                  if(left !== 0){
                        if(chessboard[this.left-1][this.top-1] !== null){
                              if(chessboard[this.left-1][this.top-1].isWhite !== this.isWhite){
                                    if(ifCanMove(this.isWhite, this.left-1, this.top-1, this.left, this.top)){return true;}
                              }
                        }
                  }
                  if(left !== 7){
                        if(chessboard[this.left+1][this.top-1] !== null){
                              if(chessboard[this.left+1][this.top-1].isWhite !== this.isWhite){
                                    if(ifCanMove(this.isWhite, this.left+1, this.top-1, this.left, this.top)){return true;}
                              }
                        }
                  }
            }
            else{
                  if(chessboard[this.left][this.top+1] == null){
                        if(ifCanMove(this.isWhite, this.left, this.top+1, this.left, this.top)){return true;}
                        if(!this.hasMoved && chessboard[this.left][this.top+2] == null){
                              if(ifCanMove(this.isWhite, this.left, this.top+2, this.left, this.top)){return true;}
                        }
                  }
                  if(left !== 0){
                        if(chessboard[this.left-1][this.top+1] !== null){
                              if(chessboard[this.left-1][this.top+1].isWhite !== this.isWhite){
                                    if(ifCanMove(this.isWhite, this.left-1, this.top+1, this.left, this.top)){return true;}
                              }
                        }
                  }
                  if(left !== 7){
                        if(chessboard[this.left+1][this.top+1] !== null){
                              if(chessboard[this.left+1][this.top+1].isWhite !== this.isWhite){
                                    if(ifCanMove(this.isWhite, this.left+1, this.top+1, this.left, this.top)){return true;}
                              }
                        }    
                  }             
            }
            return false;
      }
}


/**
 * Schachbrett erstellen
 */
var chessboard = [];

for (var i = 0; i < 8; i++) {
chessboard.push([i])
      for (var j = 0; j < 8; j++) {
      chessboard[i][j] = null;
      }
}

chessboard[0][0] = new Rook(false, 0, 0);
chessboard[1][0] = new Knight(false, 1, 0);
chessboard[2][0] = new Bishop(false, 2, 0);
chessboard[3][0] = new Queen(false, 3, 0);
chessboard[4][0] = blackKing;
chessboard[5][0] = new Bishop(false, 5, 0);
chessboard[6][0] = new Knight(false, 6, 0);
chessboard[7][0] = new Rook(false, 7, 0);

for(var i = 0;i < 8;i++){
      chessboard[i][1] = new Pawn(false, i, 1);
}

for(var i = 0;i < 8;i++){
      chessboard[i][6] = new Pawn(true, i, 6);
}

chessboard[0][7] = new Rook(true, 0, 7);
chessboard[1][7] = new Knight(true, 1, 7);
chessboard[2][7] = new Bishop(true, 2, 7);
chessboard[3][7] = new Queen(true, 3, 7);
chessboard[4][7] = whiteKing;
chessboard[5][7] = new Bishop(true, 5, 7);
chessboard[6][7] = new Knight(true, 6, 7);
chessboard[7][7] = new Rook(true, 7, 7);

/**
 * Make buttons glow
 */
function analysisMode(){
      document.getElementById("button1").style.backgroundColor = "lightblue";
      document.getElementById("button2").style.backgroundColor = "white";
}

function playMode(){
      document.getElementById("button1").style.backgroundColor = "white";
      document.getElementById("button2").style.backgroundColor = "lightblue";
}
/**
 * Paint the Square
 */
function paintSquare(left, top){
      let id = (left + 1)+(top*8);
      id = id + "";
      document.getElementById(id).style.backgroundColor = "green";
}
/**
 * Paint everything in correct color
 */
function repaintBoard(){
            let squareId = 0;
            let field;
      for(let file = 0; file < 8; file++){
            for(let rank = 0; rank < 8; rank++){
                  squareId++;
                  field = squareId + "";
                  if((file + rank) % 2 == 0){
                        document.getElementById(field).style.backgroundColor = "white";
                  }
                  else{
                        document.getElementById(field).style.backgroundColor = "black";
                  }
            }
      }
}
/**
 * If piece gets dropped
 */
function allowDrop(ev) {
      ev.preventDefault();
}
/**
 * If piece gets dragged
 */
function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
      startLeft = (ev.target.parentNode.id - 1) % 8 ;
      startTop = (ev.target.parentNode.id - 1 - startLeft) / 8;
      chessboard[startLeft][startTop].showPossibleMoves();
}

/**
 * When piece gets dropped
 */
function drop(event) {
      event.preventDefault();
      var data = event.dataTransfer.getData("text");
      var el = event.target.parentNode;
      if(event.target.style.backgroundColor == "green" || el.style.backgroundColor == "green"){
            if(event.target.tagName.toLowerCase() == 'img'){
                  if(document.getElementById(data).classList[0] != event.target.classList[0]){
                        event.target.remove();
                        el.appendChild(document.getElementById(data));
                        if(whitesMove){
                        whitesMove = false;
                        }
                        else{
                        whitesMove = true;
                        }
                  }
            }
            else{
                  event.target.appendChild(document.getElementById(data));
                  if(whitesMove){
                        whitesMove = false;
                  }
                  else{
                        whitesMove = true;
                  }
            }
            disablePieces();
            //Target Stimmt
            let targetLeft;
            let targetTop;
            if(event.target.tagName.toLowerCase() == 'img'){
                  targetLeft = (el.id - 1) % 8 ;
                  targetTop = (el.id - 1 - targetLeft) / 8;
            }
            else{
            targetLeft = (event.target.id - 1) % 8 ;
            targetTop = (event.target.id - 1 - targetLeft) / 8;
            }
            //Koordinate wird richtig eingestellt
            chessboard[startLeft][startTop].left = targetLeft;
            chessboard[startLeft][startTop].top = targetTop;
            //Target wird Figur und Startfeld wird null
            chessboard[targetLeft][targetTop] = chessboard[startLeft][startTop];
            chessboard[targetLeft][targetTop].hasMoved = true;
            chessboard[startLeft][startTop] = null;
            //Is it checkmate?
            someoneCanMove = false;
            if(whitesMove){
                  for (let i = 0; i < 8; i++) {
                        for (let j = 0; j < 8; j++) {
                              if(chessboard[i][j] !== null){
                                    if(chessboard[i][j].isWhite){
                                          if(chessboard[i][j].canMove()){
                                                someoneCanMove = true;
                                          }
                                    }
                              }
                        }
                  }
                  if(!someoneCanMove){
                        if(whiteKing.isInCheck()){
                              setTimeout(function(){ alert("Checkmate"); }, 10);
                        }
                        else{
                              setTimeout(function(){ alert("Patt du Loser"); }, 10); 
                        }
                  }
            }
            else{
                  for (let i = 0; i < 8; i++) {
                        for (let j = 0; j < 8; j++) {
                              if(chessboard[i][j] !== null){
                                    if(!chessboard[i][j].isWhite){
                                          if(chessboard[i][j].canMove()){
                                                someoneCanMove = true;
                                          }
                                    }
                              }
                        }
                  }
                  if(!someoneCanMove){
                        if(blackKing.isInCheck()){
                              setTimeout(function(){ alert("Checkmate"); }, 10); 
                        }
                        else{
                              setTimeout(function(){ alert("Patt du Loser"); }, 10); 
                        }
                  }
            }
      }
      repaintBoard();
      if(whiteKing.isInCheck()){
            let id = (whiteKing.left + 1)+(whiteKing.top*8);
            id = id + "";
            document.getElementById(id).style.backgroundColor = "red";
      }
      if(blackKing.isInCheck()){
            let id = (blackKing.left + 1)+(blackKing.top*8);
            id = id + "";
            document.getElementById(id).style.backgroundColor = "red";
      }
}
/**
 * Only the Player that can move can touch his pieces. The others are disabled
 */
function disablePieces(){
      var white_piece = document.getElementsByClassName('white');
      var black_piece = document.getElementsByClassName('black');
      if(whitesMove){
            for (var i = 0; i < white_piece.length; ++i) {
            white_piece[i].setAttribute('draggable', true); 
            }
            for (var i = 0; i < black_piece.length; ++i) {
                  black_piece[i].setAttribute('draggable', false); 
            }
      }
      else{
            for (var i = 0; i < white_piece.length; ++i) {
                  white_piece[i].setAttribute('draggable', false); 
                  }
                  for (var i = 0; i < black_piece.length; ++i) {
                        black_piece[i].setAttribute('draggable', true); 
                  }
      }
}
